class TableroGato extends HTMLElement {
	
  constructor() {
	super();
	this.shadow = this.attachShadow({mode : 'open'});
	this.setEstilosInicio();
	this.setEstilosInicioHover();
	this.elementosDOM = new Array();
  }
  
  connectedCallback() {
	this.setFilas(prompt('Recuadros'));
	this.cargarTablero();
  }
  
  disconnectedCallback() {
  }
  
  attributeChangedCallback(name, oldValue, newValue) {
	if (newValue !== '3') {
	  this.eliminarElmentosTablero();
	  this.setFilas(newValue);
	  this.cargarTablero();
	}
  }
  
  static get observedAttributes() {
  	return ['side', '3'];
  }
  
  setFilas(string) {
	if (string === null || string.length === 0) {
	  this.filas = 3;
	} else {
	  const PATRON = /^[0-9]*$/
	  if (PATRON.test(string)) {
	    this.filas = parseInt(string);
	  } else {
	    throw 'NumberFormatException';
	  }
	}
  }
  
  recargarTablero() {
	this.eliminarElmentosTablero();
	this.setFilas(prompt('Recuadros'));
	this.cargarTablero();
  }

  eliminarElmentosTablero() {
	this.elementosDOM.forEach((value) => {
	  this.shadow.removeChild(value);
	});
	this.elementosDOM = new Array();
  }
  
  cargarTablero() {
	this.columnas = this.filas;
	this.tablero = new Array(this.filas);
	this.tableroCopia = new Array(this.filas);
    this.crearTablero();
  }
  
  crearTablero() {
	let table = document.createElement('table');
	let td;
	for(let i = 0; i < this.filas; i++) {
	  this.tablero[i] = new Array(this.columnas);
	  this.tableroCopia[i] = new Array(this.columnas);
	  let tr = document.createElement('tr');
	  for(let j = 0; j < this.columnas; j++) {
		this.tablero[i][j] = this.crearCasilla(i, j);
		td = document.createElement('td');
		td.appendChild(this.tablero[i][j]);
	    tr.appendChild(td);
	  }
	  table.appendChild(tr);
	}	
	let center = document.createElement('center');
	center.appendChild(table);
	this.shadow.appendChild(center);
	this.elementosDOM.push(center);
  }
  
  crearCasilla(fila, columna) {
	let btn = document.createElement('button');
	btn.textContent = '?';
	btn.addEventListener('click', () => {
	  let digito = prompt('Digita ¿O o X?');
	  digito = digito.toUpperCase();
	  if (digito === 'X' || digito === 'O') {
	    this.colocarDigito(fila, columna, digito);
	    btn.setAttribute('disabled', true);
	    btn.textContent = digito;
	  } else {
	  throw `El dígito ${digito} no es válido`;
	  }
	});
	return btn;
  }
  
  colocarDigito(fila, columna, digito) {
	this.tableroCopia[fila][columna] = digito;
	this.getGanador(this.tableroCopia, fila, columna, this.buscarEnArreglo, digito);
  }
  
  mostrarGanador(digito) {
	if (digito !== undefined) {
	  alert(`Ganador :  ${digito}`);
	  this.recargarTablero();
	}
  }
  
  getGanador(tablero, fila, columna, busqueda, digito) {
	const busqFila = this.buscarGanador(this.busquedaEnFila, tablero, fila, busqueda, digito);
	const busqColumna = this.buscarGanador(this.busquedaEnColumnas, tablero, columna, busqueda, digito);
	const busqDiagIzqDer = this.buscarGanador(this.busquedaEnDiagonalIzqDer, tablero, 0, busqueda, digito);
	const busqDiagDerIzq = this.buscarGanador(this.busquedaEnDiagonalDerIzq, tablero, this.filas, busqueda, digito);
	Promise.allSettled([busqFila, busqColumna, busqDiagIzqDer, busqDiagDerIzq])
	.then((values) => {
	  values.forEach((element, index) => {
		if (element.status === 'fulfilled') {
		  this.mostrarGanador(digito);
		}
	  });
    });
  }
  
  /**CALLBACK**/

  buscarGanador(callback, tablero, value, busqueda, digito) {
	return new Promise((resolve, reject) => {
	  let esGanador = callback(tablero, value, busqueda, digito);
	  if (esGanador) {
	    resolve(digito);
	  } else {
	    reject(digito);
	  }
	});
  }
  
  busquedaEnFila(tablero, fila, busqueda, digito) {
	return busqueda(tablero[fila], digito);
  }
  
  busquedaEnColumnas(tablero, columna, busqueda, digito) {
	let colAux = tablero.map((element, index) => {
	  return element[columna];
	});
	return busqueda(colAux, digito);
  }
  
  busquedaEnDiagonalIzqDer(tablero, value, busqueda, digito) {
	let diagonalIzquierdaDerecha = tablero.map((element, index) => {
	  return element[index];
	});
	return busqueda(diagonalIzquierdaDerecha, digito);
  }
  
  busquedaEnDiagonalDerIzq(tablero, value, busqueda, digito) {
	let diagonalDerechaIzquierda = tablero.map((element, index) => {
	  return element[(value - 1) - index];
    });
	return busqueda(diagonalDerechaIzquierda, digito);
  }
  
  buscarEnArreglo(array, digito) {
	digito = (digito === 'X') ? 'O' : 'X';
	if (!array.includes(undefined)) {
	  return !array.includes(digito);
	}
	return false;
  }
  
  /**ESTILOS**/
  setEstilosInicioHover() {
	let css = 'button:hover {background-color: rgba(0, 0,0,.5); cursor: pointer;}';
	let style = document.createElement('style');
	if(style.styleSheet) {
	  style.styleSheet.cssText = css;
	} else {
	  style.appendChild(document.createTextNode(css));
	}
	this.shadow.appendChild(style);
  }
  
  setEstilosInicio() {
	let css = 'button{ background-color: #33FF95; color: #FF3399; border: none; padding: 20px; margin: 2px; font-size: 5em; }';
	let style = document.createElement('style');
	if(style.styleSheet) {
	  style.styleSheet.cssText = css;
	} else {
	  style.appendChild(document.createTextNode(css));
	}
	this.shadow.appendChild(style);
  }
}

customElements.define('tablero-gato', TableroGato);